package com.produto.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.produto.model.Produto;

public class ProdutosRepository {

	private EntityManager manager;

	public ProdutosRepository(EntityManager manager) {
	this.manager = manager;
	}

	public List<Produto> todos() {
		TypedQuery<Produto> query = manager.createQuery("from Produto", Produto.class);
		return query.getResultList();
	}

}
