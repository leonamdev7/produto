package com.produto.resources;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.produto.model.JpaUtil;
import com.produto.model.Produto;
import com.produto.repository.ProdutosRepository;

@WebServlet("/consulta-produtos")
public class ConsultaProdutosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		EntityManager manager = JpaUtil.getEntityManager();
		ProdutosRepository produtos = new ProdutosRepository(manager);
		try {
			//TypedQuery<Produto> query = manager.createQuery("from Produto", Produto.class);
			
			List<Produto> todosProdutos = produtos.todos();
			// imprime a abertura das tags html e body
			out.println("<html>");
			out.println("<body>");
			// imprime a abertura da tag table e linha do cabeçalho
			out.println("<table border=\"1\">");
			out.println("<tr><th>Nome</th><th>Preço</th>" + "<th>Quantidade</th></tr>");
			for (Produto produto : todosProdutos) {
				out.println("<tr>");
				out.println("<td>" + produto.getNome() + "</td>");
				out.println("<td>" + produto.getPrecoCusto() + "</td>");
				out.println("<td>" + produto.getQuantidadeEstoque() + "</td>");
				out.println("</tr>");
			}
			// imprime o fechamento da tabela e das tags body e html
			out.println("</table>");
			out.println("</body>");
			out.println("</html>");
		} finally {
			manager.close();
		}
	}
}